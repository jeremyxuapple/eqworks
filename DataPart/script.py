import pandas as pd
from math import sin, cos, sqrt, atan2, radians, pi

df_POI = pd.read_csv('./POIList.csv')
df_DS = pd.read_csv('./DataSample.csv')

# Data clean up, remove the data with same latitude and longitude

df_POI = df_POI.drop_duplicates([' Latitude', 'Longitude'])

# Average Radius of Earch in kilometers
R = 6373

distanceArr = []

# Creating distance data frame for later analysis
for index, row in df_POI.iterrows():
    lat1 = radians(row[' Latitude'])
    lon1 = radians(row['Longitude'])
    col = 'Distance' + str(index+1)
    d = []
    for j, r in df_DS.iterrows():
        lat2 = radians(r['Latitude'])
        lon2 = radians(r['Longitude'])

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c

        d.append(distance)

    distanceArr.append(d)

df = pd.DataFrame(data=distanceArr)
length = df_POI['POIID'].count()
# Find out the closed point of interest of each points in the data sample, and create a new column to hold the index of
# the close point of interest
df.loc[length] = df.idxmin()

# Transpose the dataframe
dfT = df.transpose()

# quantile parameter is the value to filter out points which are too far from the POI, 0.75 will only keep the points
# distance is smaller than 75th-percentile of the whole set
quantile = 0.75

print('\nWhile set the Quantile is ' + str(quantile) + ' we have the result as below: \n')
print('****************************************')
print('*****************RESULT*****************')
print('****************************************\n')


# Iterate through all the point of interest and perform the calculation
for x in range(length):
#     x is the index of the point of interest, select the points which are belong to itself, as the points are mark the
# cloest POI in the last colum
    df = dfT.loc[dfT[length] == x]
#  set the boundary base on the quantile to filter out the points which are too far from the POI
    boundary = df[x].quantile(quantile)
    df_filtered = df[df[x] < boundary]

#     df_filtered is the final data frame we need to analyze on
    print('The average distance of POI' + str(x+1) + ' is ' + str(df_filtered[x].mean()))
    print('The standard deviation of distance of POI' + str(x+1) + ' is ' + str(df_filtered[x].std()))
    radius = df_filtered[x].max()
    area = pi * radius**2
    density = df_filtered[x].count() / area
    print('The radius of POI' + str(x+1) + ' area is ' + str(radius) + ' km(s)')
    print ('Density of POI' + str(x + 1) + ' is ' + str(density))
    print("\n")
