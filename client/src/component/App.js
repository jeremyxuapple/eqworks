import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import Header from './Header';
import EventsDaily from './events/EventsDaily';
import EventsHourly from './events/EventsHourly';
import StatsHourly from './stats/StatsHourly';
import StatsDaily from './stats/StatsDaily';

import '../styles/App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <Header></Header>
          <Route exact={true} path='/' component={ StatsDaily }></Route>
          <Route exact path='/events/daily' component={ EventsDaily }></Route>
					<Route exact path='/events/hourly' component={ EventsHourly }></Route>
          <Route exact path='/stats/daily' component={ StatsDaily }></Route>
        </div>
      </BrowserRouter>

    );
  }
}

export default App;
