import React,  { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as actions from '../../action/';
import randomstring from 'randomstring';
import RC2 from 'react-chartjs2';

import StatsHourly from './StatsHourly';

class StatsDaily extends Component {

	componentWillMount() {
		this.props.fetchStatsDaily();
		this.props.fetchStatsHourly();
	}

	daysData = [];

 options = {
  tooltips: {
    mode: 'label'
  },
  hover: {
    mode: 'dataset'
  },

};


	render() {
		this.generateChartsData();
		if (this.props.stats.daily !== undefined) {
			return (
				<div className="container table-responsive">
					<table className="table table-striped">
						<thead>
							<tr>
									<th>Date</th>
									<th>Clicks</th>
									<th>Impressions</th>
									<th>Revenues</th>
							</tr>
						</thead>
						<tbody>
							{this.renderStatsItems()}
						</tbody>
					</table>
				</div>
			);
		} else {
			return (
				<div>
					<h1>Welcome to EQ Works!!!</h1>
				</div>
			);
		}
	}

	renderStatsItems() {
		if (this.props.stats.daily !== undefined) {
			return(
					this.props.stats.daily.map(({ date, clicks, revenue, impressions }) => {

						const key = randomstring.generate();
						const url = '/stats/hourly/' + date;

						return(
							<tr key={key}>
									<td>
										<a className='navbar-brand'>
										 {date.slice(0,10)}
									 	</a>
									  <RC2 data={ this.grabChartData(date) } options={this.options} type='line' />
										<RC2 data={ this.grabImpressionData(date) } options={this.options} type='line' />
									</td>
									<td>{parseFloat(clicks).toLocaleString('en')}</td>
									<td>{parseFloat(impressions).toLocaleString('en')}</td>
									<td>${parseFloat(revenue).toLocaleString('en')}</td>
							</tr>
						)
					})
				);
		}

	}

	generateChartsData() {
		// console.log(this.props.stats.daily);
		if (this.props.stats.daily !== undefined) {
			for (var i = 0; i < this.props.stats.hourly.length; i += 24) {
					// slice the array into days, day1, day2 ..., each day will plot
					// one chart
			    var day = this.props.stats.hourly.slice(i, i + 24);

					var revenues = [];
					var clicks = [];
					var impressions = [];

					day.forEach((data) => {
						revenues.push(parseFloat(data.revenue).toFixed(2));
						clicks.push(data.clicks);
						impressions.push(data.impressions);
					});

					var oneDayData = {
						date: this.props.stats.hourly[i].date,
						oneDayRevenues: revenues,
						oneDayClicks: clicks,
						oneDayImpressions: impressions
					}

				this.daysData.push(oneDayData);
			}
			// console.log(this.daysData);
		}
	}

	chartLabels () {
		var foo = [];
		for (var i = 0; i <= 24; i++) {
			 foo.push(i);
		 }
		 return foo;
	}

	grabChartData(date){
		const selectedDateData = this.daysData.filter((oneDayData) => {
			if( oneDayData.date === date) return true;
		});

		return ({
				labels: this.chartLabels(),
				datasets: [
					{
						label: "One Day Revenue",
						borderColor : 'rgb(255, 0, 0)',
						backgroundColor : 'rgb(255, 0, 0)',
						pointBorderColor : 'rgb(0, 0, 255)',
						pointBackgroundColor : 'rgb(255, 255, 0)',
					  pointBorderWidth : 1,
						fill: false,
						data: selectedDateData[0].oneDayRevenues
					},
					{
						label: "One Day Clicks",
						borderColor : 'rgb(255, 123, 0)',
						backgroundColor : 'rgb(255, 123, 0)',
						pointBorderColor : 'rgb(0, 0, 255)',
						pointBackgroundColor : 'rgb(255, 255, 0)',
					  pointBorderWidth : 1,
						fill: false,
						data: selectedDateData[0].oneDayClicks
					},
				]
			});
	}

	grabImpressionData(date) {
		const selectedDateData = this.daysData.filter((oneDayData) => {
			if( oneDayData.date === date) return true;
		});

		return ({
				labels: this.chartLabels(),
				datasets: [
					{
						label: "One Day Impressions",
						borderColor : 'rgb(124, 252, 0)',
						backgroundColor : 'rgb(124, 252, 0)',
						pointBorderColor : 'rgb(0, 0, 255)',
						pointBackgroundColor : 'rgb(255, 255, 0)',
						pointBorderWidth : 1,
						fill: false,
						data: selectedDateData[0].oneDayImpressions
					},
				]
			});
	}

}

function mapStateToProps(state) {
	return { stats: state.stats }
}


export default connect(mapStateToProps, actions)(StatsDaily);
