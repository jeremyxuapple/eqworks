import React,  { Component } from 'react';

import { connect } from 'react-redux';
import randomstring from 'randomstring';

import RC2 from 'react-chartjs2';

import * as actions from '../../action/';

class StatsHourly extends Component {

	componentWillMount() {

	}

	labels () {
		var foo = [];
		for (var i = 0; i <= 24; i++) {
		   foo.push(i);
		 }
		 return foo;
	}

	chartData (data)  {
		return({
				labels: this.labels(),
				datasets: [
					{
						label: "One Day Revenue",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgb(255, 99, 132)",
						pointColor: "#EEE",
						pointStrokeColor: "#EEE",
						pointHighlightFill: "#EEE",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: data.oneDayRevenues
					},
					{
						label: "One Day Clicks",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: data.oneDayClicks
					},
					{
						label: "One Day Impressions",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: data.oneDayImpressions
					},
				]
			});
	};

	chartOptions = {};

	generateCharts() {
		var daysData = [];

		for (var i = 0; i < this.props.stats.length; i += 24) {
				// slice the array into days, day1, day2 ..., each day will plot
				// one chart
		    var day = this.props.stats.slice(i, i + 24);

				var revenues = [];
				var clicks = [];
				var impressions = [];

				day.forEach((data) => {
					revenues.push(data.revenue);
					clicks.push(data.clicks);
					impressions.push(data.impressions);
				});

				var oneDayData = {
					oneDayRevenues: revenues,
					oneDayClicks: clicks,
					oneDayImpressions: impressions
				}

			daysData.push(oneDayData);
		}

		return (
			daysData.map(( oneDayData ) => {
				const key = randomstring.generate();
				return <RC2 key={key} data={ this.chartData(oneDayData) } options={this.chartOptions} type='line' />
			})
		);
	}


	render() {
    if (this.props.stats !== null) {

      return (
				<div>
					{this.generateCharts()}
				</div>
			);
    } else {
      return (<div></div>);
    }

	}

}

function mapStateToProps(state) {

	return { stats: state.stats }
}

export default connect(mapStateToProps, actions)(StatsHourly);
