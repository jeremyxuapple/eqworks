import React,  { Component } from 'react';
import Pagination from "react-js-pagination";
import { connect } from 'react-redux';
import randomstring from 'randomstring';

import * as actions from '../../action/';

class EventsHourly extends Component {

	constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
			pageSize: 10,
    };
  }

	componentWillMount() {
		this.props.fetchEventsHourly();
	}

	handlePageChange(pageNumber) {
	 this.setState({activePage: pageNumber});
 }

	render() {
    if (this.props.events !== null) {
      return (
        <div className="container table-responsive">
	        <table className="table table-striped">
            <thead>
	            <tr>
	                <th>Events</th>
	                <th>Hour</th>
	                <th>Date</th>
	            </tr>
            </thead>
            <tbody>
              {this.renderEventsItems()}
            </tbody>
          </table>
					<Pagination
	          activePage={this.state.activePage}
	          itemsCountPerPage={this.state.pageSize}
	          totalItemsCount={this.props.events.length}
	          pageRangeDisplayed={5}
	          onChange={(pageNumber) => {
							this.handlePageChange(pageNumber);
						}}
	        />
        </div>
    );
    } else {
      return (<div></div>);
    }
	}

  renderEventsItems() {

		const pageCount = Math.ceil(this.props.events.length / this.state.pageSize);
		const startIndex = parseInt(this.state.activePage - 1) * parseInt(this.state.pageSize);
		const endIndex = parseInt(startIndex) + parseInt(this.state.pageSize) ;

		const paginatedRows = this.props.events.slice(startIndex, endIndex);

    return(
				paginatedRows.map(({ events, hour, date }) => {
					const key = randomstring.generate();
					return(
              <tr key={key}>
                  <td>{events}</td>
                  <td>{hour}</td>
                  <td>{date.slice(0,10)}</td>
              </tr>
					)
				})
			);
  }

}

function mapStateToProps(state) {
	return { events: state.events }
}

export default connect(mapStateToProps, actions)(EventsHourly);
