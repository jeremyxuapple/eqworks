import React,  { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../action/';
import randomstring from 'randomstring';

class EventsDaily extends Component {

	componentWillMount() {
		this.props.fetchEventsDaily();
	}

	render() {
		// console.log(this.props);
		if (this.props.events !== null) {
			return (
				<div className="container table-responsive">
					<table className="table table-striped">
						<thead>
							<tr>
									<th>Date</th>
									<th>Events</th>
							</tr>
						</thead>
						<tbody>
							{this.renderEventsItems()}
						</tbody>
					</table>
				</div>
			);
		} else {
			return(<div></div>);
		}

	}

	renderEventsItems() {

		return(
				this.props.events.map(({ events, date }) => {
					const key = randomstring.generate();
					return(
							<tr key={key}>
									<td>{date.slice(0,10)}</td>
									<td>{events}</td>

							</tr>
					)
				})
			);
	}

}

function mapStateToProps(state) {
	return { events: state.events }
}


export default connect(mapStateToProps, actions)(EventsDaily);
