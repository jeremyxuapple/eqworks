import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
	return(
		<nav className="navbar navbar-default navbar-fixed-top">
      <div className="container">
        <div className="navbar-header">
					<Link className='navbar-brand' to = '/events/daily'>
					 Events Daily
					</Link>
					<Link className='navbar-brand' to = '/events/hourly'>
					 Events Hourly
					</Link>
					<Link className='navbar-brand' to = '/stats/daily'>
					 Stats Daily
					</Link>
        </div>
      </div>
    </nav>
		);
}

export default Header;
