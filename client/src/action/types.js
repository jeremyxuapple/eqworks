export const FETCH_EVENTS_DAILY = 'fetch_events_daily';
export const FETCH_EVENTS_HOURLY = 'fetch_events_hourly';
export const FETCH_STATS_DAILY = 'fetch_stats_daily';
export const FETCH_STATS_HOURLY = 'fetch_stats_hourly';
