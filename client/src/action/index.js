import axios from 'axios';

import { FETCH_EVENTS_DAILY,
 FETCH_EVENTS_HOURLY,
 FETCH_STATS_DAILY,
 FETCH_STATS_HOURLY} from './types';

export const fetchEventsDaily = () => async dispatch => {
	const res = await axios.get('/api/events/daily');
	dispatch({ type: FETCH_EVENTS_DAILY, payload: res.data });
}

export const fetchEventsHourly = () => async dispatch => {
  const pageSize = 10;
  const pageNum = 2;
  const url = '/api/events/hourly';
  const paginationUrl = '/api/events/hourly/?pageSize=' + pageSize + '&pageNum=' + pageNum;

	const res = await axios.get(url);
	dispatch({ type: FETCH_EVENTS_HOURLY, payload: res.data });
}

export const fetchStatsDaily = () => async dispatch => {
	const res = await axios.get('/api/stats/daily');
	dispatch({ type: FETCH_STATS_DAILY, payload: res.data });
}

export const fetchStatsHourly = () => async dispatch => {
  const pageSize = 10;
  const pageNum = 2;

  const url = '/api/stats/hourly';
  const paginationUrl = '/api/stats/hourly/?pageSize=' + pageSize + '&pageNum=' + pageNum;

	const res = await axios.get(url);
	dispatch({ type: FETCH_STATS_HOURLY, payload: res.data });
}
