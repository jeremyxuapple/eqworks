import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import './styles/index.css';

import reducer from './reducer';
import App from './component/App';


const store = createStore(reducer, {}, applyMiddleware(reduxThunk));


ReactDOM.render(
  <Provider store={store}><App /></Provider>,
   document.getElementById('root'));
