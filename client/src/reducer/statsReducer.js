import { FETCH_STATS_DAILY, FETCH_STATS_HOURLY } from '../action/types';

export default function(state = {}, action) {
  switch(action.type) {
    case FETCH_STATS_DAILY:
      state['daily'] = action.payload;
      return state;
    case FETCH_STATS_HOURLY:
      state['hourly'] = action.payload;
      return state;
    default:
      return state;
  }
}
