import { combineReducers } from 'redux';

import eventsReducer from './eventsReducer';
import statsReducer from './statsReducer';

export default combineReducers({
	events: eventsReducer,
	stats: statsReducer,
});
