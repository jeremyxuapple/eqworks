import { FETCH_EVENTS_DAILY, FETCH_EVENTS_HOURLY } from '../action/types';

export default function(state = null, action) {
  
  switch(action.type) {
    case FETCH_EVENTS_DAILY:
      return action.payload;
    case FETCH_EVENTS_HOURLY:
      return action.payload;
    default:
      return state;
  }
}
