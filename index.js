const express = require('express');
const pg = require('pg');
const keys = require('./config/keys');

// one minute 100 requetes are allowed for the route of '/' home page.
const rateLimit = 100;
const limitRefreshMins = 1;
var rateLimitMarker = rateLimit;

const app = express();

var config = {
  user: keys.PGUSER,
  database: keys.PGDATABASE,
  host: keys.PGHOST,
  password: keys.PGPASSWORD,
  port: keys.PGPORT
};

const pool = new pg.Pool(config);

const queryHandler = (req, res, next) => {
  pool.query(req.sqlQuery).then((r) => {

    const pageSize = req.query.pageSize;
    const pageNum = req.query.pageNum;

    if (pageNum !== undefined && pageSize !== undefined) {
      // formatt the result from the database query
      const pageCount = Math.ceil(r.rows.length / pageSize);
      const startIndex = parseInt(pageNum - 1) * parseInt(pageSize);
      const endIndex = parseInt(startIndex) + parseInt(pageSize) - 1;

      const paginatedRows = r.rows.slice(startIndex, endIndex);
      return res.json(paginatedRows || []);
    }

    return res.json(r.rows || []);

  }).catch(next)
}
  // Reset the rateLimit every one minute
  setInterval( function() {

    rateLimitMarker = rateLimit;

  }, limitRefreshMins * 60 * 1000);

  app.get('/', (req, res, next) => {

    if (rateLimitMarker > 0) {
      res.send('Welcome to EQ Works 😎');
      rateLimitMarker -= 1;
      next();
    } else {
      //
      res.status(429).send('You have reach the request rate limit, please wait for another ' + limitRefreshMins + ' minute(s) to visit');
    }

  })



app.get('/api/events/hourly', (req, res, next) => {

  req.sqlQuery = `
    SELECT date, hour, SUM(events) AS events
    FROM public.hourly_events
    GROUP BY date, hour
    ORDER BY date, hour
    LIMIT 168;
  `
  return next()
}, queryHandler)

app.get('/api/events/daily', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, SUM(events) AS events
    FROM public.hourly_events
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `
  return next()
}, queryHandler)

app.get('/api/stats/hourly', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, hour,
        SUM(impressions) AS impressions,
        SUM(clicks) AS clicks,
        SUM(revenue) AS revenue
    FROM public.hourly_stats
    GROUP BY date, hour
    ORDER BY date, hour
    LIMIT 168;
  `
  return next()
}, queryHandler)

app.get('/api/stats/daily', (req, res, next) => {
  req.sqlQuery = `
    SELECT date,
        SUM(impressions) AS impressions,
        SUM(clicks) AS clicks,
        SUM(revenue) AS revenue
    FROM public.hourly_stats
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `
  return next()
}, queryHandler)

app.listen(process.env.PORT || 5555, (err) => {
  if (err) {
    console.error(err)
    process.exit(1)
  } else {
    console.log(`Running on ${process.env.PORT || 5555}`)
  }
})

// last resorts
process.on('uncaughtException', (err) => {
  console.log(`Caught exception: ${err}`)
  process.exit(1)
})
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
  process.exit(1)
})
